package com.example.demo.aop;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aop")
public class AopController {

    @Log
    @RequestMapping("/log")
    public String testLog() {
        System.out.println("2333");
        return "hello aop";
    }
}
